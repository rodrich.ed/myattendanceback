<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Models\Student;
use App\Services\Interfaces;

use App\Services\Interfaces\IStudentServiceInterface;

class StudentServiceImpl implements Interfaces\IStudentServiceInterface {
    private $model;
    function  __construct(){
        $this->model = new Student();
    }


    function getStudent()
    {
        return $this->model->get();

    }

    function getStudentById(int $id)
    {
        return $this->model->where('id',$id)->get();

    }

    function postStudent(array $announcement)
    {
        $this->model->create($announcement);
    }

    function putStudent(array $announcement, int $id)
    {
        $this->model->where('id',$id)
            ->first()
            ->fill($announcement)
            ->save();
    }

    function delStudent(int $id)
    {
        $announcement=$this->model->where('id',$id)
            ->first();
        if($announcement != null){
            $announcement->delete();
        }

    }

    function restoreStudent(int $id)
    {
        // TODO: Implement restoreAnnouncement() method.
    }
}

