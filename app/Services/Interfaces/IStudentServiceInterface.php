<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IStudentServiceInterface
{
    function getStudent();
    /**
     *
     * @param int $id
     * @return Announcement
     */
    function getStudentById(int $id);
    /**
     *
     * @param array $announcement
     * @return void
     */
    function postStudent(array $announcement);
    function putStudent(array $announcement, int $id);
    function delStudent(int $id);


    function restoreStudent(int $id);
//   function getMegamenu();

}

