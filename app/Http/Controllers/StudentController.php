<?php

namespace App\Http\Controllers;

use App\Services\Implementation\StudentServiceImpl;
use App\Validator\AnnouncementValidator;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
class StudentController extends Controller
{
    /**
     *
     * @var StudentServiceImpl
     */
    private $announcementService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;

    public function __construct(StudentServiceImpl $announcementService, Request $request, AnnouncementValidator $validator){
        $this->announcementService = $announcementService;
        $this->request = $request;
        $this->validator=$validator;
    }
    function getListStudent(){
        return response($this->announcementService->getStudent());
    }
    function createStudent(){
        try{
		$this->announcementService->postStudent($this->request->all());
		return new JsonResponse(['message' => trans('accept')],200);

	}catch(Exception $e){
		Log::error($e->getResponse());
		return new JsonResponse(['message'=>trans('Ocurrio un problema')],500);

	}
    }
    function deleteStudent(int $id){
        $this->announcementService->delStudent($id);
        return response("",204);
    }
    function updateStudent($id){
        $response = response("",202);
        $this->announcementService->putStudent($this->request->all(), $id);
        return $response;
    }
    function getByIdStudent($id){
        return response($this->announcementService->getStudentById($id));
    }


}
