<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix" => "/v1"], function () use ($router)
{
    $router->group(["prefix" => "/attendance"], function () use ($router)
    {
        $router->post('', 'AttendanceController@getListAttendance');
        $router->post('createAttendance', 'AttendanceController@createAttendance');
        $router->delete('/delete/{id}', 'AttendanceController@deleteAttendance');
        $router->put('/update/{id}', 'AttendanceController@updateAttendance');
        $router->get("/find/{id}",'AttendanceController@getByIdAttendance');
    });
    $router->group(["prefix" => "/student"], function () use ($router)
    {
        $router->get('getStudent', 'StudentController@getListStudent');
        $router->post('createStudent', 'StudentController@createStudent');
        $router->delete('/delete/{id}', 'StudentController@deleteStudent');
        $router->put('/update/{id}', 'StudentController@updateStudent');
        $router->get("/find/{id}",'StudentController@getByIdStudent');
    });
});
